package com.androtush.tracking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private EditText mEdtMobileNumber;
    private Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
        listner();

    }

    private void init() {

        mEdtMobileNumber = (EditText) findViewById(R.id.edtMobileNumber);
        mBtnNext = (Button) findViewById(R.id.btnNext);
    }


    private void listner() {

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    Intent intent = new Intent(MainActivity.this,OTPActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    mEdtMobileNumber.setError("Please enter valid mobile number");
                    Toast.makeText(MainActivity.this,"Please enter valid mobile number",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean validate(){
      if(mEdtMobileNumber.getText().toString().length() == 10)
          return true;
      else
          return false;
    }

}
