package com.androtush.tracking;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class OTPActivity extends AppCompatActivity {

    private EditText mEdtOTP;
    private Button mBtnNext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        init();
        listner();

    }

    private void init() {

        mEdtOTP = (EditText) findViewById(R.id.edtOTP);
        mBtnNext = (Button) findViewById(R.id.btnNext);
    }


    private void listner() {

        mBtnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    Intent intent = new Intent(OTPActivity.this,MapsActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    mEdtOTP.setError("Please enter valid OTP");
                    Toast.makeText(OTPActivity.this,"Please enter valid OTP",Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private boolean validate(){
        if(mEdtOTP.getText().toString().equals("3271"))
            return true;
        else
            return false;
    }
}
